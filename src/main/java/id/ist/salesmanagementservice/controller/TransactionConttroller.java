package id.ist.salesmanagementservice.controller;

import id.ist.salesmanagementservice.domain.Product;
import id.ist.salesmanagementservice.dto.ResponseStatus;
import id.ist.salesmanagementservice.dto.product.ProductDto;
import id.ist.salesmanagementservice.dto.product.RequestPaginationProduct;
import id.ist.salesmanagementservice.dto.product.ViewPaginationProduct;
import id.ist.salesmanagementservice.dto.transaction.TransactionRequest;
import id.ist.salesmanagementservice.repository.ProductRepository;
import id.ist.salesmanagementservice.service.SalesManagementService;
import id.ist.salesmanagementservice.service.SalesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static id.ist.salesmanagementservice.controller.PathConstants.*;

@RestController
public class TransactionConttroller {

    private ProductRepository productRepository;

    @Autowired
    private SalesManagementService salesManagementService;

    @Autowired
    public TransactionConttroller(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

//    @Autowired
//    private StuffManagementService stuffManagementService;

    @Autowired
    private SalesService salesService;

//    @GetMapping(PRODUCT)
//    public Page<Product> findAll(Pageable pageable) {
//        return productRepository.findAll(pageable);
//    }

//    @PostMapping(UPDATE_PRODUCT)
//    public ResponseStatus updateProduct(@RequestBody ProductDto request) {
//        return stuffManagementService.saveOrUpdateProduct(request);
//    }
//
//    @PostMapping(INSERT_PRODUCT)
//    public ResponseStatus insertProduct(@RequestBody ListDataProductDto request) {
//        return stuffManagementService.InsertProduct(request);
//    }
//
//    @PostMapping(UPDATE_AMOUNT_PRODUCT)
//    public ResponseStatus updateAmountProduct(@RequestBody ProductDto request) {
//        return stuffManagementService.InsertAmountProduct(request);
//    }

    @PostMapping(PAYMENT_TRANSACTION)
    public ResponseStatus newTransactionProduct(@RequestBody TransactionRequest request) {
        if (request.getPembeliId() == null) {
            return salesService.addTransactionProductv3(request);
        }
        return salesService.addTransactionProductv2(request);
    }
//
//    @PostMapping(INQUIRY_TRANSACTION)
//    public ResponseStatus inquiryTransaction(@RequestBody TransactionRequest request) {
//        return salesService.inquiryTransaction(request);
//    }

//    @PostMapping(VIEW_PRODUCT)
//    public ViewPaginationProduct listProduct(@RequestBody RequestPaginationProduct request) {
//        return stuffService.getProduct(request);
//    }

    @PostMapping(VIEW_PRODUCT)
    public ViewPaginationProduct listProductv1(@RequestBody RequestPaginationProduct request) {
        return salesManagementService.listProduct(request);
    }

    @PostMapping(GET_PRODUCT)
    public Product getProduct(@RequestBody ProductDto request) {
        return salesManagementService.getProduct(request.getId());
    }
}
