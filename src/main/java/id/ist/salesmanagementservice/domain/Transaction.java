package id.ist.salesmanagementservice.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "transaction")
public class Transaction {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "transaction_id", unique = true, nullable = false, length = 11)
    private String id;

//    @NotNull
//    @Column(name = "id_transaction", length = 36, nullable = false)
//    private String transactionId;

//    @NotNull
//    @Column(name = "product_id", length = 36, nullable = false)
//    private String productId;

    @NotNull
    @Column(name = "pembeli_id", length = 36, nullable = false)
    private String pembeliId;

    @NotNull
    @Column(name = "employee_id", length = 36, nullable = false)
    private String employeeId;

    @NotNull
    @Column(name = "status_id", nullable = false)
    private String statusId;

    @NotNull
    @Column(name = "total_price", nullable = false)
    private Long totalPrice;

    @NotNull
    @Column(name = "begining_balance", nullable = false)
    private Long beginingBalance;

    @NotNull
    @Column(name = "ending_balance", nullable = false)
    private Long endingBalance;

    @NotNull
    @Column(name = "type_transaction", nullable = false)
    private String typeTransaction;

    @NotNull
    @Column(name = "status_code", nullable = false)
    private String statusCode;

    @NotNull
    @Column(name = "status_desc", nullable = false)
    private String statusDesc;

//    @ManyToOne
//    @JoinColumn(name = "status_id", nullable = false)
//    private StatusTransaction status;

    @CreatedDate
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @Column(name = "created_date")
//   private Instant createdDate = Instant.now();
    private Date createdDate;
}
