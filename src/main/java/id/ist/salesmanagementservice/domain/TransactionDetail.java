package id.ist.salesmanagementservice.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "transaction_detail")
public class TransactionDetail implements Serializable {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "id", nullable = false, length = 36)
    private String id;

    @NotNull
    @Column(name = "id_transaction", length = 36, nullable = false)
    private String idTransaction;

    @NotNull
    @Column(name = "product_id", length = 36, nullable = false)
    private String productId;

    @NotNull
    @Column(name = "amount_product", nullable = false)
    private String amountProduct;

//    @NotNull
//    @Column(name = "price_product", nullable = false)
//    private Long priceProduct;
//
//    @NotNull
//    @Column(name = "begining_balance", nullable = false)
//    private Long beginingBalance;
//
//    @NotNull
//    @Column(name = "ending_balance", nullable = false)
//    private Long endingBalance;

    @CreatedDate
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @Column(name = "created_date")
//   private Instant createdDate = Instant.now();
    private Date createdDate;
}
