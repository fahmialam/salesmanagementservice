package id.ist.salesmanagementservice.dto.transaction;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TransactionRequest {

    private String pembeliId;
    private String employeeId;
    private String customerName;
    private String customerEmail;
    private Long balance;
    private List<TransactionDto> transaction;
}
