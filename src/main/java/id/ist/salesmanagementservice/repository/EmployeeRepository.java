package id.ist.salesmanagementservice.repository;

import id.ist.salesmanagementservice.domain.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface EmployeeRepository extends JpaRepository<Employee, String> {

    @Query("SELECT a from Employee a where a.id = :id")
    Employee getById (String id);
}
