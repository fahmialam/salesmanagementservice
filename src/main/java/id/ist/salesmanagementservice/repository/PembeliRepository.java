package id.ist.salesmanagementservice.repository;

import id.ist.salesmanagementservice.domain.Pembeli;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PembeliRepository extends JpaRepository<Pembeli, String> {

    @Query("SELECT a from Pembeli a where a.id = :id")
    Pembeli getById (String id);

    @Query("SELECT a from Pembeli a where a.name = :name")
    Pembeli getByName (String name);
}
