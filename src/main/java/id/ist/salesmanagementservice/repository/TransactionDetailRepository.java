package id.ist.salesmanagementservice.repository;

import id.ist.salesmanagementservice.domain.TransactionDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionDetailRepository extends JpaRepository<TransactionDetail, String> {
}
