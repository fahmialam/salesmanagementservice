package id.ist.salesmanagementservice.repository;

import id.ist.salesmanagementservice.domain.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionRepository extends JpaRepository<Transaction, String> {
}
