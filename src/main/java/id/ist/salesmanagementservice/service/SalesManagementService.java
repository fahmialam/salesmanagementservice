package id.ist.salesmanagementservice.service;

import id.ist.salesmanagementservice.domain.Pembeli;
import id.ist.salesmanagementservice.domain.Product;
import id.ist.salesmanagementservice.dto.ResponseStatus;
import id.ist.salesmanagementservice.dto.product.ListDataProductDto;
import id.ist.salesmanagementservice.dto.product.ProductDto;
import id.ist.salesmanagementservice.dto.product.RequestPaginationProduct;
import id.ist.salesmanagementservice.dto.product.ViewPaginationProduct;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Log4j2
@Service
public class SalesManagementService {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${saveOrUpdateProduct}")
    private String saveOrUpdateProduct;

    @Value("${getListProduct}")
    private String getListProduct;

    @Value("${getProductById}")
    private String getProductById;

    @Value("${insertCustomer}")
    private String insertCustomer;

    public ViewPaginationProduct listProduct(RequestPaginationProduct request) {
        return restTemplate.postForObject(getListProduct, request, ViewPaginationProduct.class);
    }

    public ResponseStatus updateProduct(ListDataProductDto request) {

        log.info("### call api UPDATE_PRODUCT");

        return restTemplate.postForObject(saveOrUpdateProduct, request, ResponseStatus.class);
    }

    public Product getProduct(String id) {

        log.info("### call api GET_PRODUCT");

        ResponseEntity<Product> responseEntity = restTemplate.getForEntity(getProductById + "/?id=" + id,
                Product.class);
        return responseEntity.getBody();
    }

    public ResponseStatus insertCustomer(Pembeli request) {

        log.info("### call api INSERT_CUSTOMER");

        return restTemplate.postForObject(insertCustomer, request, ResponseStatus.class);
    }
}
