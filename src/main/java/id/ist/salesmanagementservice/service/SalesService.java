package id.ist.salesmanagementservice.service;

import id.ist.salesmanagementservice.domain.*;
import id.ist.salesmanagementservice.dto.ResponseStatus;
import id.ist.salesmanagementservice.dto.product.ListDataProductDto;
import id.ist.salesmanagementservice.dto.product.ProductDto;
import id.ist.salesmanagementservice.dto.receipt.ReceiptTransaction;
import id.ist.salesmanagementservice.dto.transaction.CekDto;
import id.ist.salesmanagementservice.dto.transaction.TransactionDto;
import id.ist.salesmanagementservice.dto.transaction.TransactionRequest;
import id.ist.salesmanagementservice.repository.*;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Log4j2
@Service
public class SalesService {

    @Autowired
    private TransactionRepository transactionRepository;

//    @Autowired
//    private ProductRepository productRepository;

    @Autowired
    private PembeliRepository pembeliRepository;

    @Autowired
    private TransactionDetailRepository statusTransactionRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private SalesManagementService salesManagementService;

//    @Autowired
//    private JavaMailSender sender;
//
//    @Autowired
//    private Configuration configuration;

    public static Date dateCurrentDate() {
        TimeZone timeZone = TimeZone.getTimeZone("Asia/Jakarta");
        Calendar calendar = Calendar.getInstance(timeZone);
        Date date = calendar.getTime();
        return date;
    }

//    public ResponseStatus addTransactionProductv1(TransactionRequest request) {
//
//        //email
////        MimeMessage message = sender.createMimeMessage();
//////        List<ResponseMail> responseMails = new ArrayList<>();
////        List<ListProduct> listProducts = new ArrayList<>();
//        String statusCode = ""; // 01 -> catch excaption, 02 -> id not fund
//
//        Pembeli dataPembeli = pembeliRepository.getById(request.getPembeliId());
//        List<Product> productList = new ArrayList<>();
//        List<ProductDto> dataproduct = new ArrayList<>();
//        List<Transaction> transactions = new ArrayList<>();
//
//        DateFormat dayhourminute = new SimpleDateFormat("ddHHmm");
//        DateFormat mountyear = new SimpleDateFormat("MM/yyyy");
//
//        if (dataPembeli == null) {
//            return ResponseStatus.builder()
//                    .responseCode("01")
//                    .responseMessage("Id Pembeli Tidak Ada")
//                    .build();
//        }
//
//        try {
//
//            TransactionDto tran = TransactionDto.builder()
//                    .amount(0L)
//                    .build();
//
//            for (TransactionDto data : request.getTransaction()) {
//
//                Product product = productRepository.getById(data.getProductId());
//
//                tran = TransactionDto.builder()
//                        .amount(tran.getAmount() + (data.getAmount() * product.getPriceProduct()))
//                        .build();
//
//                long productData = product.getAmount() - data.getAmount();
//
//                if (productData < 0) {
//
//                    CekDto cekDto = CekDto.builder()
//                            .begining(String.valueOf(product.getAmount()))
//                            .ending(String.valueOf(productData))
//                            .amountTransaction(String.valueOf(data.getAmount()))
//                            .build();
//
//                    return ResponseStatus.builder()
//                            .responseCode("01")
//                            .responseMessage("Stok Tidak Cukup / habis")
//                            .content(cekDto)
//                            .build();
//                }
//
//                product.setAmount(product.getAmount() - data.getAmount());
//                productList.add(product);
//
//                Transaction transaction = new Transaction();
//                transaction.setCreatedDate(dateCurrentDate());
//                transaction.setStatusId(dayhourminute.format(dateCurrentDate()).concat("/TRANSACTION/")
//                        .concat(mountyear.format(dateCurrentDate())));
//                transaction.setEmployeeId(request.getEmployeeId());
////                transaction.setProductId(data.getProductId());
//                transaction.setPembeliId(request.getPembeliId());
////                transaction.setTransactionId("00");
//                log.info("### data transaction : " + transaction);
//                transactions.add(transaction);
//
//                ProductDto productDto = ProductDto.builder()
//                        .name(product.getNameProduct())
//                        .price(product.getPriceProduct())
//                        .amount(data.getAmount())
//                        .total(data.getAmount() * product.getPriceProduct())
//                        .build();
//                dataproduct.add(productDto);
//
//                //email
////                ListProduct listProduct = ListProduct.builder()
////                        .product(product.getNameProduct().concat(" x ").concat(String.valueOf(data.getAmount()).concat(" @").concat(String.valueOf(product.getPriceProduct()))))
////                        .build();
////                listProducts.add(listProduct);
//            }
//
//            TransactionDetail statusTran = new TransactionDetail();
//
//            statusTran.setId(dayhourminute.format(dateCurrentDate()).concat("/TRANSACTION/")
//                    .concat(mountyear.format(dateCurrentDate())));
////            statusTran.setStatus("00");
////            statusTran.setDescription("SUCCESS");
//            statusTran.setCreatedDate(dateCurrentDate());
////            statusTran.setPriceProduct(tran.getAmount());
////            statusTran.setBeginingBalance(dataPembeli.getBalance());
////            statusTran.setEndingBalance(dataPembeli.getBalance() - tran.getAmount());
//
//            long balance = dataPembeli.getBalance() - tran.getAmount();
//            if (balance < 0) {
//
//                CekDto cekDto = CekDto.builder()
//                        .begining(String.valueOf(dataPembeli.getBalance()))
//                        .ending(String.valueOf(balance))
//                        .amountTransaction(String.valueOf(tran.getAmount()))
//                        .build();
//
//                return ResponseStatus.builder()
//                        .responseCode("01")
//                        .responseMessage("Saldo Tidak Cukup")
//                        .content(cekDto)
//                        .build();
//            }
//
//            dataPembeli.setBalance(dataPembeli.getBalance() - tran.getAmount());
//
//            log.info("### data pemeberli" + dataPembeli.getBalance());
//            log.info("### data tran : " + tran);
//
////            productRepository.saveAll(productList);
////            pembeliRepository.save(dataPembeli);
////            transactionRepository.saveAll(transactions);
////            statusTransactionRepository.save(statusTran);
//
//            Employee employee = employeeRepository.getById(request.getEmployeeId());
//
//            ReceiptTransaction receiptTransaction = ReceiptTransaction.builder()
//                    .idTransaction(statusTran.getId())
//                    .date(String.valueOf(dateCurrentDate()))
//                    .pegawai(employee.getName())
//                    .pembeli(dataPembeli.getName())
//                    .listProduct(dataproduct)
//                    .build();
//
//            //email
////            ResponseMail responseMail = ResponseMail.builder()
////                    .listProducts(listProducts)
////                    .build();
////
////            String dataMail = String.valueOf(responseMail.getListProducts());
////            Map<String, Object> model = new HashMap<>();
////            String regex = "\\[|\\]";
////            String regex1 = "\\)";
////            model.put("name", dataPembeli.getName());
////            model.put("product", dataMail.replaceAll(regex, "")
////                    .replaceAll("ListProduct[(]product=", "<br>").replaceAll(regex1,""));
////
////            model.put("tranId", statusTran.getId());
////            model.put("dateTime", receiptTransaction.getDate());
////            model.put("total", tran.getAmount());
////
////            if (!receiptTransaction.equals("null")) {
////                // set mediaType
////                MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
////                        StandardCharsets.UTF_8.name());
////                Template t = configuration.getTemplate("email-template1.ftl");
////                String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, model);
////
////                helper.setTo(dataPembeli.getEmail());
////                log.info("### helper email to : " + dataPembeli.getEmail());
////
////                helper.setSubject("Bukti Transaction IST");
////                helper.setFrom(employee.getEmail());
////                log.info("### helper email from : " + employee.getEmail());
////                helper.setText(html);
////                sender.send(message);
////            }
//
//
//
//
//            return ResponseStatus.builder()
//                    .responseCode("00")
//                    .responseMessage("Success")
//                    .content(receiptTransaction)
//                    .build();
//
//        } catch (Exception e) {
//            return ResponseStatus.builder()
//                    .responseCode("01")
//                    .responseMessage("Error Transaction")
//                    .build();
//        }
//    }

    public ResponseStatus addTransactionProductv2(TransactionRequest request) {

        //email
//        MimeMessage message = sender.createMimeMessage();
////        List<ResponseMail> responseMails = new ArrayList<>();
//        List<ListProduct> listProducts = new ArrayList<>();
        String statusCode = ""; // 01 -> catch excaption, 02 -> id not fund

        Pembeli dataPembeli = pembeliRepository.getById(request.getPembeliId());
        List<ProductDto> productList = new ArrayList<>();
        List<ProductDto> dataProductResi = new ArrayList<>();
        List<TransactionDetail> listTranDetail = new ArrayList<>();

        DateFormat dayhourminute = new SimpleDateFormat("ddHHmm");
        DateFormat mountyear = new SimpleDateFormat("MM/yyyy");

//        if (dataPembeli == null) {
//            return ResponseStatus.builder()
//                    .responseCode("01")
//                    .responseMessage("Id Pembeli Tidak Ada")
//                    .build();
//        }

        try {

            TransactionDto tran = TransactionDto.builder()
                    .amount(0L)
                    .build();

            for (TransactionDto data : request.getTransaction()) {

                Product product = salesManagementService.getProduct(data.getProductId());

                log.info("### responseStatus getProduct : " + product);

                tran = TransactionDto.builder()
                        .amount(tran.getAmount() + (data.getAmount() * product.getPriceProduct()))
                        .build();

                long productData = product.getAmount() - data.getAmount();

                if (productData < 0) {

                    CekDto cekDto = CekDto.builder()
                            .begining(String.valueOf(product.getAmount()))
                            .ending(String.valueOf(productData))
                            .amountTransaction(String.valueOf(data.getAmount()))
                            .build();

                    return ResponseStatus.builder()
                            .responseCode("01")
                            .responseMessage("Stok Tidak Cukup / habis")
                            .content(cekDto)
                            .build();
                }

                ProductDto productDto = ProductDto.builder()
                        .id(data.getProductId())
                        .amount(product.getAmount() - data.getAmount())
                        .build();

                log.info("### responseStatus productDto amount : " + productDto.getAmount());

                productList.add(productDto);

                TransactionDetail tranDetail = TransactionDetail.builder()
                        .idTransaction(dayhourminute.format(dateCurrentDate()).concat("/TRANSACTION/")
                                .concat(mountyear.format(dateCurrentDate())))
                        .productId(data.getProductId())
                        .amountProduct(String.valueOf(data.getAmount()))
                        .createdDate(dateCurrentDate())
                        .build();
                listTranDetail.add(tranDetail);

                ProductDto productDtoResi = ProductDto.builder()
                        .name(product.getNameProduct())
                        .price(product.getPriceProduct())
                        .amount(data.getAmount())
                        .total(data.getAmount() * product.getPriceProduct())
                        .build();
                dataProductResi.add(productDtoResi);

                //email
//                ListProduct listProduct = ListProduct.builder()
//                        .product(product.getNameProduct().concat(" x ").concat(String.valueOf(data.getAmount()).concat(" @").concat(String.valueOf(product.getPriceProduct()))))
//                        .build();
//                listProducts.add(listProduct);
            }

            Transaction transaction = Transaction.builder()
                    .createdDate(dateCurrentDate())
                    .pembeliId(request.getPembeliId())
                    .employeeId(request.getEmployeeId())
                    .statusId(dayhourminute.format(dateCurrentDate()).concat("/TRANSACTION/")
                            .concat(mountyear.format(dateCurrentDate())))
                    .totalPrice(tran.getAmount())
                    .beginingBalance(dataPembeli.getBalance())
                    .endingBalance(dataPembeli.getBalance() - tran.getAmount())
                    .statusCode("00")
                    .statusDesc("SUCCESS")
                    .typeTransaction("saldo")
                    .build();

            log.info("### data transaction : " + transaction);

            long balance = dataPembeli.getBalance() - tran.getAmount();
            if (balance < 0) {

                CekDto cekDto = CekDto.builder()
                        .begining(String.valueOf(dataPembeli.getBalance()))
                        .ending(String.valueOf(balance))
                        .amountTransaction(String.valueOf(tran.getAmount()))
                        .build();

                return ResponseStatus.builder()
                        .responseCode("01")
                        .responseMessage("Saldo Tidak Cukup")
                        .content(cekDto)
                        .build();
            }

            dataPembeli.setBalance(dataPembeli.getBalance() - tran.getAmount());

            log.info("### data pemeberli" + dataPembeli.getBalance());
            log.info("### data tran : " + tran);

            ResponseStatus responseStatus = salesManagementService.updateProduct(ListDataProductDto.builder()
                    .product(productList)
                    .build());

            log.info("### responseStatus updateProduct : " + responseStatus);

            if (responseStatus.getResponseCode().equals("00")) {

                pembeliRepository.save(dataPembeli);
                transactionRepository.save(transaction);
                statusTransactionRepository.saveAll(listTranDetail);
            }

            Employee employee = employeeRepository.getById(request.getEmployeeId());

            //RECEIPT
            ReceiptTransaction receiptTransaction = ReceiptTransaction.builder()
                    .idTransaction(transaction.getStatusId())
                    .date(String.valueOf(dateCurrentDate()))
                    .pegawai(employee.getName())
                    .pembeli(dataPembeli.getName())
                    .listProduct(dataProductResi)
                    .build();

            //email
//            ResponseMail responseMail = ResponseMail.builder()
//                    .listProducts(listProducts)
//                    .build();
//
//            String dataMail = String.valueOf(responseMail.getListProducts());
//            Map<String, Object> model = new HashMap<>();
//            String regex = "\\[|\\]";
//            String regex1 = "\\)";
//            model.put("name", dataPembeli.getName());
//            model.put("product", dataMail.replaceAll(regex, "")
//                    .replaceAll("ListProduct[(]product=", "<br>").replaceAll(regex1,""));
//
//            model.put("tranId", statusTran.getId());
//            model.put("dateTime", receiptTransaction.getDate());
//            model.put("total", tran.getAmount());
//
//            if (!receiptTransaction.equals("null")) {
//                // set mediaType
//                MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
//                        StandardCharsets.UTF_8.name());
//                Template t = configuration.getTemplate("email-template1.ftl");
//                String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, model);
//
//                helper.setTo(dataPembeli.getEmail());
//                log.info("### helper email to : " + dataPembeli.getEmail());
//
//                helper.setSubject("Bukti Transaction IST");
//                helper.setFrom(employee.getEmail());
//                log.info("### helper email from : " + employee.getEmail());
//                helper.setText(html);
//                sender.send(message);
//            }




            return ResponseStatus.builder()
                    .responseCode("00")
                    .responseMessage("Success")
                    .content(receiptTransaction)
                    .build();

        } catch (Exception e) {
            return ResponseStatus.builder()
                    .responseCode("01")
                    .responseMessage("Error Transaction")
                    .build();
        }
    }

    public ResponseStatus addTransactionProductv3(TransactionRequest request) {

        //email
//        MimeMessage message = sender.createMimeMessage();
////        List<ResponseMail> responseMails = new ArrayList<>();
//        List<ListProduct> listProducts = new ArrayList<>();
//        String statusCode = ""; // 01 -> catch excaption, 02 -> id not fund

//        Pembeli dataPembeli = pembeliRepository.getById(request.getPembeliId());
        List<ProductDto> productList = new ArrayList<>();
        List<ProductDto> dataProductResi = new ArrayList<>();
        List<TransactionDetail> listTranDetail = new ArrayList<>();

        DateFormat dayhourminute = new SimpleDateFormat("ddHHmm");
        DateFormat mountyear = new SimpleDateFormat("MM/yyyy");

//        if (dataPembeli == null) {
//            return ResponseStatus.builder()
//                    .responseCode("01")
//                    .responseMessage("Id Pembeli Tidak Ada")
//                    .build();
//        }

        try {

            TransactionDto tran = TransactionDto.builder()
                    .amount(0L)
                    .build();

            for (TransactionDto data : request.getTransaction()) {

                Product product = salesManagementService.getProduct(data.getProductId());

                log.info("### responseStatus getProduct : " + product);

                tran = TransactionDto.builder()
                        .amount(tran.getAmount() + (data.getAmount() * product.getPriceProduct()))
                        .build();

                long productData = product.getAmount() - data.getAmount();

                if (productData < 0) {

                    CekDto cekDto = CekDto.builder()
                            .begining(String.valueOf(product.getAmount()))
                            .ending(String.valueOf(productData))
                            .amountTransaction(String.valueOf(data.getAmount()))
                            .build();

                    return ResponseStatus.builder()
                            .responseCode("01")
                            .responseMessage("Stok Tidak Cukup / habis")
                            .content(cekDto)
                            .build();
                }

                ProductDto productDto = ProductDto.builder()
                        .id(data.getProductId())
                        .amount(product.getAmount() - data.getAmount())
                        .build();

                log.info("### responseStatus productDto amount : " + productDto.getAmount());

                productList.add(productDto);

                TransactionDetail tranDetail = TransactionDetail.builder()
                        .idTransaction(dayhourminute.format(dateCurrentDate()).concat("/TRANSACTION/")
                                .concat(mountyear.format(dateCurrentDate())))
                        .productId(data.getProductId())
                        .amountProduct(String.valueOf(data.getAmount()))
                        .createdDate(dateCurrentDate())
                        .build();
                listTranDetail.add(tranDetail);

                ProductDto productDtoResi = ProductDto.builder()
                        .name(product.getNameProduct())
                        .price(product.getPriceProduct())
                        .amount(data.getAmount())
                        .total(data.getAmount() * product.getPriceProduct())
                        .build();
                dataProductResi.add(productDtoResi);

                //email
//                ListProduct listProduct = ListProduct.builder()
//                        .product(product.getNameProduct().concat(" x ").concat(String.valueOf(data.getAmount()).concat(" @").concat(String.valueOf(product.getPriceProduct()))))
//                        .build();
//                listProducts.add(listProduct);
            }

            ResponseStatus responseStatusPembeli = salesManagementService.insertCustomer(Pembeli.builder()
                    .name(request.getCustomerName())
                    .email(request.getCustomerEmail())
                    .build());

            if (responseStatusPembeli.getResponseCode().equals("01")){

                return ResponseStatus.builder()
                        .responseCode("01")
                        .responseMessage("gagal menambahkan customer")
                        .build();
            }

            Pembeli pembeli = pembeliRepository.getByName(request.getCustomerName());

            if (pembeli == null) {
                return ResponseStatus.builder()
                        .responseCode("01")
                        .responseMessage("id gagalllll")
                        .build();
            }

            Transaction transaction = Transaction.builder()
                    .createdDate(dateCurrentDate())
                    .pembeliId(pembeli.getId())
                    .employeeId(request.getEmployeeId())
                    .statusId(dayhourminute.format(dateCurrentDate()).concat("/TRANSACTION/")
                            .concat(mountyear.format(dateCurrentDate())))
                    .totalPrice(tran.getAmount())
                    .beginingBalance(request.getBalance())
                    .endingBalance(request.getBalance() - tran.getAmount())
                    .typeTransaction("cash")
                    .statusCode("00")
                    .statusDesc("SUCCESS")
                    .build();

            log.info("### id pemebeli : " + transaction.getPembeliId());

            log.info("### data transaction : " + transaction);

            log.info("### data tran : " + tran);

            ResponseStatus responseStatus = salesManagementService.updateProduct(ListDataProductDto.builder()
                    .product(productList)
                    .build());

            log.info("### responseStatus updateProduct : " + responseStatus);

            if (responseStatus.getResponseCode().equals("00")) {

                transactionRepository.save(transaction);
                statusTransactionRepository.saveAll(listTranDetail);
            }

            Employee employee = employeeRepository.getById(request.getEmployeeId());

            //RECEIPT
            ReceiptTransaction receiptTransaction = ReceiptTransaction.builder()
                    .idTransaction(transaction.getStatusId())
                    .date(String.valueOf(dateCurrentDate()))
                    .pegawai(employee.getName())
//                    .pembeli(dataPembeli.getName())
                    .pembeli(request.getCustomerName())
                    .change(request.getBalance() - tran.getAmount())
                    .listProduct(dataProductResi)
                    .build();

            //email
//            ResponseMail responseMail = ResponseMail.builder()
//                    .listProducts(listProducts)
//                    .build();
//
//            String dataMail = String.valueOf(responseMail.getListProducts());
//            Map<String, Object> model = new HashMap<>();
//            String regex = "\\[|\\]";
//            String regex1 = "\\)";
//            model.put("name", dataPembeli.getName());
//            model.put("product", dataMail.replaceAll(regex, "")
//                    .replaceAll("ListProduct[(]product=", "<br>").replaceAll(regex1,""));
//
//            model.put("tranId", statusTran.getId());
//            model.put("dateTime", receiptTransaction.getDate());
//            model.put("total", tran.getAmount());
//
//            if (!receiptTransaction.equals("null")) {
//                // set mediaType
//                MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
//                        StandardCharsets.UTF_8.name());
//                Template t = configuration.getTemplate("email-template1.ftl");
//                String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, model);
//
//                helper.setTo(dataPembeli.getEmail());
//                log.info("### helper email to : " + dataPembeli.getEmail());
//
//                helper.setSubject("Bukti Transaction IST");
//                helper.setFrom(employee.getEmail());
//                log.info("### helper email from : " + employee.getEmail());
//                helper.setText(html);
//                sender.send(message);
//            }




            return ResponseStatus.builder()
                    .responseCode("00")
                    .responseMessage("Success")
                    .content(receiptTransaction)
                    .build();

        } catch (Exception e) {
            return ResponseStatus.builder()
                    .responseCode("01")
                    .responseMessage("Error Transaction")
                    .build();
        }
    }

    public ResponseStatus inquiryTransaction(TransactionRequest request) {

        try {

            if (request.getTransaction().size() == 0) {
                return ResponseStatus.builder()
                        .responseCode("01")
                        .responseMessage("Error Transaction")
                        .build();
            }

            if (StringUtils.isEmpty(request.getPembeliId())) {
                return ResponseStatus.builder()
                        .responseCode("01")
                        .responseMessage("Customer null")
                        .build();
            }

            return ResponseStatus.builder()
                    .responseCode("00")
                    .responseMessage("Success")
                    .build();
        } catch (Exception e) {
            return ResponseStatus.builder()
                    .responseCode("01")
                    .responseMessage("Error Transaction")
                    .build();
        }
    }
}
